use core::time;
use std::{fs::metadata, path::Path, thread};
use walkdir::WalkDir;
use cached::proc_macro::cached;
use convert_case::{Casing, Case};
use id3::{Tag, TagLike, Version};
use scraper::{Html, Selector};
use clap::Parser;
use anyhow::{Result, Context};

fn main() -> Result<()> {

    /// Change genre tags using RateYourMusic
    #[derive(Parser, Debug)]
    #[clap(author, version, about, long_about = None)]
    struct Args {
        /// Search through folders recursively? 
        #[clap(short, long, value_parser, default_value_t = false)]
        recursive: bool,

        // Path to file or folder
        #[clap(short, long, value_parser)]
        path: String
    }

    let args = Args::parse();

    // Check if path argument is directory
    match metadata(&args.path).unwrap().is_dir() {
        // If directory...
        true => {
            // Setup recursive search
            let mut walk = WalkDir::new(args.path);
            // Set recursion level
            if !args.recursive { walk = walk.max_depth(0); }

            for entry in walk {
                // For each item found, check if it's a file
                if entry.as_ref().ok().unwrap().path().is_file() {
                    // Only lookup files
                    if let Err(e) = lookup( entry?.path()) {println!("Failed to look up track: {}", e)}
                }
            }
        },
        // If file, just lookup file
        false => { lookup(Path::new(&args.path) ).unwrap() },
    }

    Ok(())
}

// Perform a search and replace genre tags
fn lookup(file: &Path) -> Result<()> {

    let mut tag = Tag::read_from_path(&file)?;

    // RYM formats artists and such in kebab-case, and without symbols
    let artist = tag.album_artist().unwrap()
        .to_case(Case::Kebab).replace(&['(', ')', ',', '\"', '.', ';', ':', '\''][..], "");

    let album = tag.album().unwrap() 
        .to_case(Case::Kebab).replace(&['(', ')', ',', '\"', '.', ';', ':', '\''][..], "");

    // Template URL
    let url = format!("https://rateyourmusic.com/release/album/{artist}/{album}/");

    // Fetch genres from RYM and set genre tag
    //todo: send as list of genres instead of merging string
    tag.set_genre(fetch_genres(url).context("Failed to fetch RYM info")?);
    tag.write_to_path(&file, Version::Id3v24)?;

    // Show this information for terminal users
    println!("{}: {}", (format!("{}",file.display()).split('/').collect::<Vec<&str>>().pop().unwrap()), tag.genre().unwrap());

    Ok(())
}

// Performs a search on RYM for the album
#[cached(result = true)]
fn fetch_genres(url: String) -> Result<String> {
    let body: String = ureq::get(url.as_str())
        .set("User-Agent", "rym-lookup from codeberg.org/j0")
        .call()?
        .into_string()?;

    // Be nice :) 
    thread::sleep(time::Duration::from_secs(2));


    let document = Html::parse_document(&body);
    let selector = Selector::parse("tr.release_genres td div span a").unwrap();

    let mut v: Vec<String> = document.select(&selector)
        .map(|i| i.inner_html()).collect();

    let selector = Selector::parse("tr.release_descriptors td div span").unwrap();
    for i in document.select(&selector) {
        for i in i.inner_html().split(",  ") {
            if i.is_empty() {break;}
            v.push(String::from(i));
        }
    }

    Ok(v.join(";"))
}